package com.android.adiproductreview.viewmodel

import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.adiproductreview.network.ProductDetailModel
import com.android.adiproductreview.network.RetroInstance
import com.android.adiproductreview.network.RetroService
import com.android.adiproductreview.network.ReviewsInfo
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ProductDetailViewModel : ViewModel() {

    var productDetail: MutableLiveData<ArrayList<ProductDetailModel>>
    var productReviews: MutableLiveData<ArrayList<ReviewsInfo>>
    var review : MutableLiveData<ReviewsInfo>

    init {
        productDetail = MutableLiveData()
        productReviews = MutableLiveData()
        review = MutableLiveData()
    }

    fun getProductDetailObserver(): MutableLiveData<ArrayList<ProductDetailModel>> {
        return productDetail;
    }

    fun getProductReviewsObserver(): MutableLiveData<ArrayList<ReviewsInfo>> {
        return productReviews
    }

    fun addReviewObserver(): MutableLiveData<ReviewsInfo>{
        return review
    }

    fun fetchProductById(id:String, progressBar: ProgressBar) {

        val retroInstance  = RetroInstance.getProductRetroInstance().create(RetroService::class.java)
        retroInstance.getProductById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getProductListById(progressBar))
    }

    private fun getProductListById(progressBar: ProgressBar):Observer<ProductDetailModel> {
        val list = ArrayList<ProductDetailModel>()
        return object :Observer<ProductDetailModel>{
            override fun onComplete() {
                progressBar.visibility = View.GONE
            }

            override fun onError(e: Throwable) {
                Log.e("ProductList On Error: " , e.message)
                progressBar.visibility = View.GONE
                productDetail.postValue(null)
            }

            override fun onNext(t: ProductDetailModel) {
                Log.e("ProductList : " , t.toString())
                list.add(t)
                productDetail.postValue(list)
            }

            override fun onSubscribe(d: Disposable) {
                //start showing progress indicator.
                progressBar.visibility = View.VISIBLE
            }
        }
    }

    fun addReview(review: ReviewsInfo, progressBar: ProgressBar) {

        val retroInstance  = RetroInstance.getReviewRetroInstance().create(RetroService::class.java)
        retroInstance.addProductReview(review)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onReviewAdded(progressBar))
    }

    fun fetchReviews(id : String, progressBar: ProgressBar){
        val retroInstance  = RetroInstance.getReviewRetroInstance().create(RetroService::class.java)
        retroInstance.getProductReviews(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onReviewFetched(progressBar))
    }

    private fun onReviewFetched(progressBar: ProgressBar): Observer<ArrayList<ReviewsInfo>> {
        return object : Observer<ArrayList<ReviewsInfo>> {
            override fun onComplete() {
                //hide progress indicator .
                progressBar.visibility = View.GONE
            }

            override fun onError(e: Throwable): Unit {
                progressBar.visibility = View.GONE
                productReviews.postValue(null)
            }

            override fun onNext(t: ArrayList<ReviewsInfo>) {
                productReviews.postValue(t)
            }

            override fun onSubscribe(d: Disposable) {
                //start showing progress indicator.
                progressBar.visibility = View.VISIBLE
            }
        }
    }

    private fun onReviewAdded(progressBar: ProgressBar): Observer<ReviewsInfo> {
        return object : Observer<ReviewsInfo> {
            override fun onComplete() {
                //hide progress indicator .
                progressBar.visibility = View.GONE
            }

            override fun onError(e: Throwable) {
                progressBar.visibility = View.GONE
                review.postValue(null)
            }

            override fun onNext(t: ReviewsInfo) {
                review.postValue(t)
            }

            override fun onSubscribe(d: Disposable) {
                //start showing progress indicator.
                progressBar.visibility = View.VISIBLE
            }
        }
    }
}
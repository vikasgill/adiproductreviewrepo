package com.android.adiproductreview.viewmodel

import android.util.*
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.adiproductreview.network.ProductDetailModel
import com.android.adiproductreview.network.RetroInstance
import com.android.adiproductreview.network.RetroService
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainActivityViewModel: ViewModel() {
    var productList: MutableLiveData<ArrayList<ProductDetailModel>>

    init {
        productList = MutableLiveData()
    }

    fun getProductListObserver(): MutableLiveData<ArrayList<ProductDetailModel>> {
        return productList
    }

    fun fetchProductList(progressBar: ProgressBar){
        val retroInstance  = RetroInstance.getProductRetroInstance().create(RetroService::class.java)
        retroInstance.getProductListFromApi()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(getProductListObserverRx(progressBar))
    }

    private fun getProductListObserverRx(progressBar: ProgressBar):Observer<ArrayList<ProductDetailModel>> {
        return object :Observer<ArrayList<ProductDetailModel>>{
            override fun onComplete() {
                //hide progress indicator .
                progressBar.visibility = View.GONE
            }

            override fun onError(e: Throwable) {
                Log.e("ProductList On Error: " , e.message)
                progressBar.visibility = View.GONE
                productList.postValue(null)
            }

            override fun onNext(t: ArrayList<ProductDetailModel>) {
                Log.e("ProductList : " , t.toString())
                productList.postValue(t)
            }

            override fun onSubscribe(d: Disposable) {
                //start showing progress indicator.
                progressBar.visibility = View.VISIBLE
            }
        }
    }
}

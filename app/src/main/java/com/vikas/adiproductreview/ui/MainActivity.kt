package com.android.adiproductreview.ui

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.adiproductreview.adapter.ProductListAdapter
import com.android.adiproductreview.network.ProductDetailModel
import com.android.adiproductreview.util.ConnectionConnectivity
import com.android.adiproductreview.viewmodel.MainActivityViewModel
import com.vikas.adiproductreviewapp.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), ProductListAdapter.OnItemClickListener {

   lateinit var viewModel:  MainActivityViewModel
    lateinit var productListAdapter: ProductListAdapter
    lateinit var  productList : ArrayList<ProductDetailModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        if(ConnectionConnectivity.isNetworkConnected(this)){
            initSearchBox()
            initRecyclerView()
            fetchProductList();

        } else{
            Toast.makeText(this, "No Internet Connectivity", Toast.LENGTH_LONG).show();
            finish()
        }

    }

    fun initSearchBox() {
        searchProduct.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                filter(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    private fun filter(value: String) {
        var list : ArrayList<ProductDetailModel> = ArrayList()

        for (product in productList){
            if(product.description.equals(value) || product.name.equals(value))
                list.add(product)
        }

        productListAdapter.productListData = list;
        productListAdapter.notifyDataSetChanged()
    }

    fun initRecyclerView(){
        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            productListAdapter = ProductListAdapter(this@MainActivity)
            adapter =productListAdapter

            //productListAdapter
        }
    }


    fun fetchProductList(){
        textViewMain.visibility = View.VISIBLE
        viewModel.getProductListObserver().observe(this, Observer<ArrayList<ProductDetailModel>> {
            if (it != null) {
                //update adapter...
                    productList = it;
                productListAdapter.productListData = it
                productListAdapter.notifyDataSetChanged()
            } else {
                onInternetUnavailable()
            }
        })
        viewModel.fetchProductList(progressBar1)
    }

    override fun onItemClicked(id: String) {
        textViewMain.visibility = View.VISIBLE
        val intent = Intent(this, ProductDetailActivity::class.java)
        intent.putExtra("id", id)
        startActivity(intent)
    }

    fun onInternetUnavailable() {
        textViewMain.visibility = View.VISIBLE
        textViewMain.setText(getString(R.string.no_internet_connection))
    }
}
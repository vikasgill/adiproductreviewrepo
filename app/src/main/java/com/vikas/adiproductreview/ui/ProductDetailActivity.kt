package com.android.adiproductreview.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.adiproductreview.adapter.ReviewListAdapter
import com.android.adiproductreview.network.ProductDetailModel
import com.android.adiproductreview.network.ReviewsInfo
import com.android.adiproductreview.viewmodel.ProductDetailViewModel
import com.bumptech.glide.Glide
import com.vikas.adiproductreviewapp.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.product_detail_activity.*
import kotlinx.android.synthetic.main.recycler_list_row.view.*
import java.util.*
import kotlin.collections.ArrayList


class ProductDetailActivity : AppCompatActivity() {

    lateinit var viewModel:  ProductDetailViewModel
    lateinit var reviewListAdapter: ReviewListAdapter

    lateinit var tvProductName: TextView
    lateinit var tvProductPrice: TextView
    lateinit var tvProductDescription: TextView
    lateinit var productImageView: ImageView
    lateinit var backButton: Button
    lateinit var addReviewButton: Button
    lateinit var productId:String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_detail_activity)

        viewModel = ViewModelProvider(this).get(ProductDetailViewModel::class.java)

        tvProductName = findViewById(R.id.productName)
        tvProductDescription = findViewById(R.id.productDescription)
        tvProductPrice = findViewById(R.id.productPrice)
        productImageView = findViewById(R.id.productImage)
        backButton = findViewById(R.id.backBtn)
        addReviewButton = findViewById(R.id.addReviewBtn)

        productId  = intent.getStringExtra("id")

        backButton.setOnClickListener(View.OnClickListener {
            finish();
        })

        addReviewButton.setOnClickListener(View.OnClickListener {
            openDialog()
        })

        initRecyclerView()
        fetchProductById(productId);

    }

    fun openDialog(){
        val builder = AlertDialog.Builder(this)

        val customLayout: View = layoutInflater
                .inflate(
                        R.layout.add_review_layout,
                        null)
        builder.setView(customLayout)


        builder.setPositiveButton(android.R.string.yes) { dialog, which ->

            val editTextReview: EditText = customLayout
                    .findViewById(
                            R.id.review)

            val editTextRating: EditText = customLayout
                    .findViewById(
                            R.id.rating)


            sendDialogDataToActivity(
                    editTextReview
                            .text
                            .toString(),
                    editTextRating
                            .text
                            .toString())
        }

        builder.setNegativeButton(android.R.string.no) { dialog, which ->
            Toast.makeText(applicationContext,
                    android.R.string.no, Toast.LENGTH_SHORT).show()
        }

        builder.setNeutralButton("Maybe") { dialog, which ->
            Toast.makeText(applicationContext,
                    "Maybe", Toast.LENGTH_SHORT).show()
        }
        builder.show()
    }

    fun sendDialogDataToActivity(review:String, rating: String){
        textViewMain.visibility = View.GONE
        var reviewInfo =  ReviewsInfo(productId, Locale.getDefault().country, rating = rating, text = review)

        viewModel.addReviewObserver().observe(this, Observer<ReviewsInfo>{
            if(it != null){
                fetchProductReviews(it.productId)}
            else{
                onInternetUnavailable()
            }
        })

        viewModel.addReview(reviewInfo, progressBar2)
    }

    fun initRecyclerView(){
        reviewsList.apply {
            layoutManager = LinearLayoutManager(this@ProductDetailActivity)
            reviewListAdapter = ReviewListAdapter()
            adapter =reviewListAdapter
        }
    }

    fun fetchProductById(id: String) {
        textViewMain.visibility = View.GONE
        viewModel.getProductDetailObserver().observe(this, Observer<List<ProductDetailModel>> {
            if (it != null) {

                //update ui

                val url = it.iterator().next().imgUrl
                Glide.with(productImageView)
                        .load(url)
                        .circleCrop()
                        .into(productImageView)

                tvProductName.text = it.iterator().next().name;
                tvProductPrice.text = it.iterator().next().price.toString();
                tvProductDescription.text = it.iterator().next().description;

                reviewListAdapter.productPastReviews = it.iterator().next().reviews

                Log.e("ProductReview Inside: ", it.iterator().next().reviews.toString())

                reviewListAdapter.createCopy();

                fetchProductReviews(id)
            } else {
                onInternetUnavailable()
            }
        })
        viewModel.fetchProductById(id, progressBar2)
    }

    fun fetchProductReviews(id: String){
        textViewMain.visibility = View.GONE
        viewModel.getProductReviewsObserver().observe(this, Observer<ArrayList<ReviewsInfo>> {
            Log.e("ProductReview : ", it.toString())
            if (it != null) {

                reviewListAdapter.productCurrentReview = it
                reviewListAdapter.copyData();
            } else {
                onInternetUnavailable()
            }
        })
        viewModel.fetchReviews(id, progressBar2)
    }

     fun onInternetUnavailable() {
        textViewMain.visibility = View.VISIBLE
        textViewProduct.setText(getString(R.string.no_internet_connection))
    }
}
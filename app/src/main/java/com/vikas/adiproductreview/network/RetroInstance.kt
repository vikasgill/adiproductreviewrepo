package com.android.adiproductreview.network

import android.content.Context
import android.net.ConnectivityManager
import com.android.adiproductreview.util.ConnectionInterceptor
import com.android.adiproductreview.util.ConnectionListener
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class RetroInstance {

    companion object {
        val baseURL = "http://localhost:3001/"
        //val baseURL = " http://7eb14cda5dc6.ngrok.io/"

        val reviewURL = "http://localhost:3002/"
        //val reviewURL = "http://1240023d9ac7.ngrok.io/"


        fun getProductRetroInstance() : Retrofit {

           return Retrofit.Builder()
                    .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        }

        fun getReviewRetroInstance(): Retrofit{

            return Retrofit.Builder()
                    .baseUrl(reviewURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
        }


    }
}
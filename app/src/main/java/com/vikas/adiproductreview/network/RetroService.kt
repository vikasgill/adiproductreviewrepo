package com.android.adiproductreview.network

import io.reactivex.Observable
import retrofit2.http.*


interface RetroService {

    @GET("product/{id}")
    fun getProductById(@Path("id") id: String): Observable<ProductDetailModel>

    @GET("product")
    fun getProductListFromApi(): Observable<ArrayList<ProductDetailModel>>

    @Headers("Content-Type: application/json")
    @POST("reviews/{id}")
    fun addProductReview(@Body reviewData: ReviewsInfo): Observable<ReviewsInfo>

    @GET("reviews/{id}")
    fun getProductReviews(@Path("id") id: String) : Observable<ArrayList<ReviewsInfo>>
}
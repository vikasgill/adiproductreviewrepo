package com.android.adiproductreview.network


data class ProductListModel(
    val detail: ArrayList<ProductDetailModel>
)

data class ProductDetailModel(
    val currency:String,
    val price: String,
    val id: String,
    val name: String,
    val description: String,
    val imgUrl: String,
    val reviews: ArrayList<ReviewsInfo>
    )
data class ReviewsInfo(
        var productId: String,
        val locale:String,
        val rating: String,
        val text: String
    )
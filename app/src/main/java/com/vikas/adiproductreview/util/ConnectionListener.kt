package com.android.adiproductreview.util

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


class ConnectionListener private constructor() {
    //this how to create our bus
    private val publisher = BehaviorSubject.create<Boolean>()
    fun notifyNetworkChange(isConnected: Boolean) {
        publisher.onNext(isConnected)
    }

    // Listen should return an Observable
    fun listenNetworkChange(): Observable<Boolean> {
        return publisher
    }

    companion object {
        private var mInstance: ConnectionListener? = null
        val instance: ConnectionListener?
            get() {
                if (mInstance == null) {
                    mInstance = ConnectionListener()
                }
                return mInstance
            }
    }
}
package com.android.adiproductreview.util

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

abstract class ConnectionInterceptor : Interceptor {
    abstract val isInternetAvailable: Boolean

    abstract fun onInternetUnavailable()
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        if (!isInternetAvailable) {
            onInternetUnavailable()
        }
        return chain.proceed(request)
    }
}
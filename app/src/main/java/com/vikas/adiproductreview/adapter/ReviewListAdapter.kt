package com.android.adiproductreview.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.adiproductreview.network.ProductDetailModel
import com.android.adiproductreview.network.ReviewsInfo
import com.bumptech.glide.Glide
import com.vikas.adiproductreviewapp.R
import kotlinx.android.synthetic.main.recycler_list_row.view.*
import kotlinx.android.synthetic.main.review_recycler_item_row.view.*

class ReviewListAdapter : RecyclerView.Adapter<ReviewListAdapter.MyViewHolder>() {

    var productPastReviews = ArrayList<ReviewsInfo>()
    var productCurrentReview = ArrayList<ReviewsInfo>()

    var productAllReviews = ArrayList<ReviewsInfo>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context).inflate(R.layout.review_recycler_item_row, parent, false )
        return MyViewHolder(inflater)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(productAllReviews[position])
    }

    override fun getItemCount(): Int {
        return productPastReviews.size + productCurrentReview.size
    }

    fun createCopy(){
        productAllReviews = ArrayList(productPastReviews)
    }

    fun copyData(){
        if(productCurrentReview != null){
            productAllReviews.addAll(productCurrentReview.filterNotNull())
        }
        notifyDataSetChanged()
    }

    class   MyViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val tvReview: TextView = view.tvReview

        fun bind(data : ReviewsInfo){
            tvReview.text = data.text
        }
    }
}
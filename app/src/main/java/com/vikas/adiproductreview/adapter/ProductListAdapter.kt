package com.android.adiproductreview.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.adiproductreview.network.ProductDetailModel
import com.bumptech.glide.Glide
import com.vikas.adiproductreviewapp.R
import kotlinx.android.synthetic.main.recycler_list_row.view.*

class ProductListAdapter(val itemClickListener: OnItemClickListener): RecyclerView.Adapter<ProductListAdapter.MyViewHolder>() {

    companion object {
        private const val PRODUCT = "Product:"
        private const val PRICE = "Price:"
        private const val DESCRIPTION = "Description:";

    }

    var productListData = ArrayList<ProductDetailModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
       val inflater = LayoutInflater.from(parent.context).inflate(R.layout.recycler_list_row, parent, false )
        return MyViewHolder(inflater)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.bind(productListData[position], itemClickListener)
    }

    override fun getItemCount(): Int {
        return productListData.size
    }

    class   MyViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val tvProductName: TextView = view.tvProductName
        private val tvProductPrice: TextView = view.tvProductPrice
        private val tvProductDescription: TextView = view.tvProductDescription
        private val productImageView: ImageView = view.thumbImageView

        fun bind(data : ProductDetailModel, clickListener: OnItemClickListener){
            tvProductName.text = ProductListAdapter.PRODUCT + " " + data.name
            tvProductPrice.text = ProductListAdapter.PRICE + " " + data.price
            tvProductDescription.text = ProductListAdapter.DESCRIPTION + " " + data.description

            val url  = data.imgUrl
            Glide.with(productImageView)
                .load(url)
                .circleCrop()
                .into(productImageView)

            itemView.setOnClickListener {
                clickListener.onItemClicked(data.id)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClicked(id: String)
    }
}